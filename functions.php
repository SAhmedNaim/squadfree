<?php

function squadFree_essential()
{
	add_theme_support( 'title-tag' );

	register_nav_menu( 'main-menu', 'Main Menu' );
}
add_action( 'after_setup_theme', 'squadFree_essential' );

function squadFree_scripts()
{
	// stylesheets
	wp_register_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.min.css' );
	wp_register_style( 'font-awesome', get_template_directory_uri().'/font-awesome/css/font-awesome.min.css' );
	wp_register_style( 'animate', get_template_directory_uri().'/css/animate.css' );
	wp_register_style( 'style', get_template_directory_uri().'/css/style.css' );
	wp_register_style( 'default', get_template_directory_uri().'/color/default.css' );


	wp_enqueue_style( 'bootstrap' );
	wp_enqueue_style( 'font-awesome' );
	wp_enqueue_style( 'animate' );
	wp_enqueue_style( 'style' );
	wp_enqueue_style( 'default' );

	// javaScripts
	wp_register_script( 'bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array('jquery') );
	wp_register_script( 'easing', get_template_directory_uri().'/js/jquery.easing.min.js', array('jquery') );
	wp_register_script( 'scrollTo', get_template_directory_uri().'/js/jquery.scrollTo.js', array('jquery') );
	wp_register_script( 'wow', get_template_directory_uri().'/js/wow.min.js', array('jquery') );
	wp_register_script( 'custom', get_template_directory_uri().'/js/custom.js', array('jquery') );
	wp_register_script( 'contactform', get_template_directory_uri().'/contactform/contactform.js', array('jquery') );
	wp_register_script( '', get_template_directory_uri().'' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap' );
	wp_enqueue_script( 'easing' );
	wp_enqueue_script( 'scrollTo' );
	wp_enqueue_script( 'wow' );
	wp_enqueue_script( 'custom' );
	wp_enqueue_script( 'contactform' );
}
add_action( 'wp_enqueue_scripts', 'squadFree_scripts' );

function default_main_menu()
{
	if(is_user_logged_in())
	{
		echo '<ul class="nav navbar-nav">';
		echo '<li><a href="' . esc_url( home_url() ) . '/wp-admin/nav-menus.php">Create a menu</a></li>';
		echo '</ul>';
	}
	else
	{
		echo '<ul class="nav navbar-nav">';
		echo '<li><a href="' . esc_url( home_url() ) . '">Home</a></li>';
		echo '</ul>';
	}
}

// include bootstrap nav walker
// require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

// include our own custom walker
require_once 'squadFree-class-walker-nav-menu.php';
